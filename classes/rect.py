from math import isinf, inf
import enum

from classes.position import Position
from helpers.math import clamp, snap

class Anchor(enum.Enum):
    left_top = 'lt'
    left_centre = 'lc'
    left_bot = 'lb'
    centre_top = 'ct'
    centre_centre = 'cc'
    centre_bot = 'cb'
    right_top = 'rt'
    right_centre = 'rc'
    right_bot = 'rb'

# a rect is a rectangular region
class Rect(object):
    x1, y1, x2, y2 = 0, 0, 0, 0

    @property
    def width(self):
        return self.x2 - self.x1 + 1

    @property
    def height(self):
        return self.y2 - self.y1 + 1
    
    def center(self, snap_type):
        return Position(snap((self.x2 - self.x1) / 2, snap_type), snap((self.y2 - self.y1) / 2, snap_type))

    @property
    def area(self):
        return self.height * self.width

    @property
    def corners(self):
        return [Position(self.x1, self.y1), Position(self.x1, self.y2), Position(self.x2,self.y2), Position(self.x2,self.y1)]

    @property
    def positions(self):
        if isinf(self.x1) or isinf(self.y1) or isinf(self.x2) or isinf(self.y2):
            return None
        positions = []
        for y in range(self.y1, self.y2 + 1):
            for x in range(self.x1, self.x2 + 1):
                positions.append(Position(x, y))
        return positions

    @property
    def outline(self):
        top = set([Position(x, self.y1) for x in range(self.x1, self.x2 + 1)])
        bot = set([Position(x, self.y2) for x in range(self.x1, self.x2 + 1)])
        left = set([Position(y, self.x1) for y in range(self.y1, self.y2 + 1)])
        right = set([Position(y, self.x2) for y in range(self.y1, self.y2 + 1)])

        return list(top & bot & left & right)

    def __init__(self, x1, y1, x2, y2):
        if x1 > x2:
            x1, x2 = x2, x1
        if y1 > y2:
            y1, y2 = y2, y1

        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    @staticmethod
    def from_anchor(x, y, width, height, anchor, snap_type):
        if isinstance(anchor, Anchor):
            anchor = anchor.value
    
        if anchor[0] == 'l':
            x1 = x
        elif anchor[0] == 'c':
            x1 = x - snap((width - 1)/2, snap_type)
        elif anchor[0] == 'r':
            x1 = x - width
        else:
            raise Exception('Unknown anchor value')

        if anchor[1] == 't':
            y1 = y
        elif anchor[1] == 'c':
            y1 = y - snap((height - 1)/2, snap_type)
        elif anchor[1] == 'b':
            y1 = y - height
        else:
            raise Exception('Unknown anchor value')

        x2 = x1 + width
        y2 = y1 + height

        return Rect(x1, y1, x2, y2)

    @staticmethod
    def infinite():
        return Rect(-inf, -inf, inf, inf)

    def contains(self, pos):
        if self.width == 0 or self.height == 0: return False
        return pos.x == clamp(pos.x, self.x1, self.x2) and pos.y == clamp(pos.y, self.y1, self.y2)

    def intersects(self, other_rect):
        for other_corner in other_rect.corners: # check for other corners in our rect
            if self.contains(other_corner):
                return True
        for corner in self.corners:
            if other_rect.contains(corner): # check for our corners in other rect
                return True
        return False