from classes.rect import Rect
from colorama import init, Back, Fore
from data.constants import COLORS_BACK, COLORS_FORE
from classes.world import World
from classes.grid import Grid
from classes.position import Position
from classes.wall import Wall


def contains_type (entities, entity_type):
    if entities == None: return False
    return any(entity.entity_type == entity_type for entity in entities)
    
def get_wall_symbol (world: World, pos: Position):
    right_entities   = world.grid.get(Position(pos.x + 1, pos.y))
    up_entities      = world.grid.get(Position(pos.x, pos.y - 1))
    left_entities    = world.grid.get(Position(pos.x - 1, pos.y))
    down_entities    = world.grid.get(Position(pos.x, pos.y + 1))

    right_val   = int(contains_type(right_entities, 'Wall'))
    up_val      = int(contains_type(up_entities, 'Wall'))
    left_val    = int(contains_type(left_entities, 'Wall'))
    down_val    = int(contains_type(down_entities, 'Wall'))

    val_list = [right_val, up_val, left_val, down_val]

    val_lookup = {
        '[0, 1, 0, 1]': '│',
        '[1, 0, 1, 0]': '─',
        '[1, 1, 0, 0]': '└',
        '[1, 0, 0, 1]': '┌',
        '[0, 0, 1, 1]': '┐',
        '[0, 1, 1, 0]': '┘',
        '[0, 1, 1, 1]': '┤',
        '[1, 1, 1, 0]': '┴',
        '[1, 0, 1, 1]': '┬',
        '[1, 1, 0, 1]': '├',
        '[1, 1, 1, 1]': '┼',
        '[0, 0, 0, 0]': 'O',
        '[1, 0, 0, 0]': '╶',
        '[0, 1, 0, 0]': '╵',
        '[0, 0, 1, 0]': '╴',
        '[0, 0, 0, 1]': '╷',
    }
    return val_lookup[repr(val_list)]

def get_primary_type (entities):
    type_priorities = ['Player', 'Wall', 'Door']

    for entity_type in type_priorities:
        if contains_type(entities, entity_type):
            return entity_type
    
    return 'None'

def get_entity_symbol (entities, world, position):
    entity_type = get_primary_type(entities)

    entity_symbols = {
        'Player': '@',
        'Wall': get_wall_symbol(world, position),
        'Door': 'x',
        'None': ' '
    }

    return entity_symbols.get(entity_type, 'INVALID_ENTITY_TYPE')

def world_foreground (world: World, frame: Rect):
    canvas = Grid()

    for y in range(frame.y1, frame.y2 + 1):
        for x in range(frame.x1, frame.x2 + 1):
            position = Position(x, y)
            entities = world.grid.get(Position(x, y))
            cell_symbol = get_entity_symbol(entities, world, position)
            canvas.add(position, cell_symbol)

    return canvas

def world_background (world: World, frame: Rect):
    canvas = Grid()

    for y in range(frame.y1, frame.y2 + 1):
        for x in range(frame.x1, frame.x2 + 1):
            position = Position(x, y)
            rooms = world.get_rooms_containing(position)

            if len(rooms) == 0:
                canvas.add(position, Back.BLACK)
            else:
                color = COLORS_BACK[sum(room.id for room in rooms) % len(COLORS_BACK)]
                canvas.add(position, color)

    return canvas