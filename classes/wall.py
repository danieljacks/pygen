from classes.body import Body

class Wall(Body):
    def __init__(self, position, materials):
        super().__init__(position, 0, 1, 1, materials)