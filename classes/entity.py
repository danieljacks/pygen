from copy import deepcopy

from classes.world import World

# an entity is an object with an id tracked by World
class Entity(object):
    # static
    __id_counter = 0

    # instanced
    __id = -1

    def __init__ (self):
        self.__id = Entity.__id_counter
        Entity.__id_counter += 1

    # id should never be mutated
    @property
    def id (self):
        if self.__id == -1:
            raise Exception('id shouldnt be -1. You probably need to call super.__init__(self) in child class constructor function.')
        return self.__id

    @property
    def entity_type (self):
        return self.__class__.__name__

    @staticmethod
    def default_entity ():
        return Entity()

    # copy this entity with a new id
    def copy(self):
        copy = deepcopy(self)
        copy.id = Entity.__id_counter
        Entity.__id_counter += 1
        return copy
