def aggregate(strings):
    counts = []
    for string in strings:
        index = -1
        for count in counts:
            if count[1] == string:
                index = counts.index(count)
        if index == -1:
            counts.append([1, string])
        else:
            counts[index][0] += 1
    return [('a' if count[0] == 1 else str(count[0])) + ' ' + (count[1]) + ('s' if count[0] > 1 else '') for count in counts]

def describe_list (strings, include_and=True, separator=',', should_aggregate=True):
    items = aggregate(strings)
    
    if len(items) == 0:
        return ''
    if len(items) == 1:
        return items[0]

    sentance = ''
    for i in range(len(items) - 2):
        sentance += items[i] + separator + ' '
    
    and_str = ' and ' if include_and else separator + ' '
    sentance += items[-2] + and_str + items[-1]

    return sentance