from house_generator import bounds_from_points, generate_house, outline
from constants import Material
from text_helpers import describe_list
from classes import World, Room, Door, Cell
from decorate import decorate_room

def house():
    cells, rooms_old, room_functions = generate_house(room_max_score=1)
    rooms = []
    for i in range(len(rooms_old)):
        room_points = [point for point in rooms_old[i]]
        room_outline = outline(room_points, include_diagonals=False)
        room_doors = []
        for outline_point in room_outline:
            if outline_point in cells and cells[outline_point] == 2:
                room_doors.append(Door(outline_point,Material.random(),[]))
        rooms.append(Room(i, room_functions[i], room_points, room_doors))
    cells_objects = {pos: Cell(cells[pos], 'wood') for pos in cells}
    for room in rooms:
        decorate_room(room)
        
    return World(cells_objects, rooms)