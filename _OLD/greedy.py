from copy import deepcopy

# implementation of greedy algorithm

def optimal_solution (initial_solution, solution_generator, score_function):
    best_solution = deepcopy(initial_solution)
    best_score = 0
    current_solution, current_score = best_solution, 1

    while current_score > best_score:
        best_solution, best_score = current_solution, current_score
        current_solution, current_score = greedy_step(current_solution, solution_generator, score_function)

    return best_solution
        

def greedy_step (current_solution, solution_generator, score_function):
    solution_options = [(solution, score_function(solution)) for solution in solution_generator(current_solution)]
    best_option = max(solution_options, key = lambda option: option[1])
    return best_option[0], best_option[1]