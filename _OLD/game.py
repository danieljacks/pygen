from gen import house
from describe import describe_room, Memory
from draw import draw_world
from classes import Player, Transform

def initialize ():
    world = house()
    start_pos = world.rooms[0].points[0]
    player = Player(Transform(start_pos, (0,1)), Memory())
    return world, player

def look_around (world, player):
    room = world.room_of(player.transform.position)
    sentences = describe_room(room)
    player.memory.sentences.extend(sentences)
    return sentences

world, player = initialize()
draw_world(world, 2, True)
sentences = look_around(world, player)
for sentence in sentences:
    print(sentence)