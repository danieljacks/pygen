from copy import deepcopy
from classes import Room, Door, Transform, Player, Entity
from gen import house
from draw import draw_world
import re
from enum import Enum
from text_helpers import describe_list

class TokenType(Enum):
    text = 0
    entity = 1

class Memory(object):
    sentences = []

class Sentence(object):
    tokens = []

    def __init__ (self, tokens):
        self.tokens = []
        for token in tokens:
            self.tokens.append(Token(token))

    def __str__ (self):
        strings = [str(token) for token in self.tokens]
        has_comma = False
        for string in strings:
            if ',' in string:
                has_comma = True
        separator = ';' if has_comma == True else ','
        describe_strings = strings[1:len(strings) - 1]
        final_string = strings[0] + describe_list(describe_strings, separator=separator) + strings[-1]
        return final_string

class Token(object):
    reference_type = TokenType.text
    reference_object = object()

    def __init__(self, reference_object):

        if isinstance(reference_object, str):
            self.reference_type = TokenType.text
        elif isinstance(reference_object, Entity):
            self.reference_type = TokenType.entity
        else:
            raise Exception('Type not implemented!')

        self.reference_object = reference_object
    
    def __str__ (self):
        return str(self.reference_object)

def describe_room (room):
    sentence1 = Sentence(['You are in ', room, '.'])
    sentence2 = Sentence(['You see ', *room.doors, '.'])
    return [sentence1, sentence2]

# house = house()
# draw_world(house)
# memory = Memory()
# transform = Transform((1, 2), (1, 0))
# for room in house.rooms[:]:
#     descs = describe_room(room)
#     for desc in descs:
#         print(desc)