from classes.position import Position
from classes.grid import Grid
from classes.rect import Rect
from draw.map import world_foreground, world_background
from colorama import Back, init
init()

def render_canvas(foreground_canvas: Grid, background_canvas: Grid):
    bounds = foreground_canvas.bounds
    # if bounds != background_canvas.bounds:
    #     raise Exception('Foreground and background canvases must be the same size')

    lines = []
    for y in range(bounds.y1, bounds.y2 + 1):
        line = ''
        for x in range(bounds.x1, bounds.x2 + 1):
            position = Position(x, y)
            line += background_canvas.get(position) or Back.WHITE
            line += ' '
            line += Back.RESET
        lines.append(line)

    return lines

def print_ui(world):
    map_frame = Rect(0, 0, 30, 30)

    map_foreground = world_foreground(world, map_frame)
    map_background = world_background(world, map_frame)

    rendered_canvas = render_canvas(map_foreground, map_background)
    for line in rendered_canvas:
        print(line)