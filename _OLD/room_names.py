from enum import Enum
from copy import deepcopy

# class RoomFunction (Enum):
#     eating = 'eating'
#     dining = 'dining'
#     sleeping = 'sleeping'
#     #  'bathroom', 'walk-in closet', 'cellar', 'office', 'ballroom'
# TODO: make perturbing raw values, using the smaller of a percentage of either of the two directions
class RoomFunction (object):
    # we need a function which takes in a number of rooms, and outputs the rooms in that house
    # we have necessary rooms (kitchen, dining bedroom, (bathroom?))
    # maybe we need certain functions, and each room type can have multiple functions. So kitchen/dining has both those functions, but is less desireable than having them separate. Also, certain rooms like to be next to other rooms (maybe a hallway for a bathroom)
    # maybe different rooms have a value funtion, and we want to maximaize this value function?
    name = ''
    initial_importance = 1
    clone_importance_multiplier = 0.3 # 
    combine_tolerance_multiplier = 0.4 # other functions in the same room are depreciate by this much

    def combine_multipler (self, functions_in_room):
        other_functions = deepcopy(functions_in_room)
        if self.name in other_functions: other_functions.remove(self.name)

        multiplier = 1
        for function in other_functions:
            multiplier *= FUNCTION_INFO[function].combine_tolerance_multiplier
        return multiplier

    def importance (self, clones_count):
        ''' the first instance of a function should have clones_count = 1'''
        multiplier_sign = 1 if self.clone_importance_multiplier >= 0 else -1
        return self.initial_importance * multiplier_sign * abs(self.clone_importance_multiplier)**(clones_count-1)

    def __init__ (self, name, initial_importance, clone_importance_multiplier, combine_tolerance_multiplier):
        self.name = name
        self.initial_importance = initial_importance
        self.clone_importance_multiplier = clone_importance_multiplier
        self.combine_tolerance_multiplier = combine_tolerance_multiplier

FUNCTION_INFO = {
    # name: name, initial_importance, clone_importance, combine_tolerance
    'kitchen':  RoomFunction('kitchen', 5, 0.05, 0.8),
    'dining':   RoomFunction('dining', 4, 0.1, 0.8),
    'bedroom':  RoomFunction('bedroom', 5, 0.7, 0.2), 
    'bathroom': RoomFunction('bathroom',5, 0.4, -0.3), 
    'storage':  RoomFunction('storage',  1, 0.7, 0.8), 
    'cellar':   RoomFunction('cellar', 0.5, 0.1, 0.6), 
    'office':   RoomFunction('office', 1.5, 0.4, 0.5), 
    'ballroom': RoomFunction('ballroom', 0.3, 0.01, 0.3), 
}

# greedy algorithm
def get_room_types (number_of_rooms):
    best_house = [[] for _ in range(number_of_rooms)]
    current_score = -99999999999999
    best_score = -99999999999998
    while best_score > current_score:
        current_house, current_score = best_house, best_score
        house_options = []
        for i in range(number_of_rooms):
            for function in FUNCTION_INFO:
                if function in current_house[i]: continue

                house_option = deepcopy(current_house)
                house_option[i].append(function)
                house_score = evaluate_rooms(house_option)
                house_options.append((house_option, house_score))
        best_option = max(house_options, key = lambda option: option[1])
        best_house, best_score = best_option[0], best_option[1]
    return best_house
    

def evaluate_rooms (rooms):
    score = 0
    room_functions_count = {function: 0 for function in FUNCTION_INFO}
    for room in rooms:
        for function in room:
            function_info = FUNCTION_INFO[function]
            # clones_count = room_functions_count[function]
            clones_count = count_functions(rooms, function)
            importance = function_info.importance(clones_count)
            combine_multipler = function_info.combine_multipler(room)
            score += importance * combine_multipler
            room_functions_count[function] += 1
    for room in rooms:
        if len(room) == 0:
            score -= 500 # prevent empty rooms
    return score
    
def count_functions (rooms, function_to_count):
    count = 0
    for room in rooms:
        for room_function in room:
            if function_to_count == room_function:
                count += 1
    return count

rooms = [
    ['kitchen', 'dining'],
]
# print(evaluate_rooms(rooms))
# print('1:', get_room_types (1))
# print('3:', get_room_types (3))
# print('4:', get_room_types (4))
# print('5:', get_room_types (5))
# print('15:', get_room_types (15))
# test_rooms1 = [['kitchen'], ['bedroom'], ['bathroom'], ['dining'], ['bedroom'], ['office'], ['bathroom'], ['storage'], ['cellar'], ['storage'], ['bedroom'], ['ballroom'], ['storage'], ['storage'], []]
# test_rooms2 = [['kitchen'], ['bedroom'], ['bathroom'], ['dining'], ['bedroom'], ['office'], ['bathroom'], ['storage'], ['cellar'], ['storage'], ['bedroom'], ['ballroom'], ['storage'], ['storage'], ['storage']]
# print('1:', evaluate_rooms(test_rooms1))
# print('2:', evaluate_rooms(test_rooms2))
# print (count_functions([['kitchen'],['kitchen','bedroom']], 'kitchen'))
# print(evaluate_rooms([['bedroom'],['bathroom'],['bedroom']]))
# print(evaluate_rooms([['bedroom'],['bathroom'],['dining']]))