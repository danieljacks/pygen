from house_generator import Bounds, outline, inline, bounds_from_points, COLORS, COLORS_FG
from copy import deepcopy
from random import randint
from math import sqrt, tanh
from colorama import init, Back, Fore
# init()

class FurnitureInfo (object):
    function = ''
    importance = 5
    anchor_point = ''
    constraints = ['']
    min_dimensions = Bounds(0, 0, 0, 0)
    max_dimensions = Bounds(1, 1, 1, 1)

    def generate_dimensions (self):
        x = randint(self.min_dimensions[0], self.max_dimensions[0])
        y = randint(self.min_dimensions[1], self.max_dimensions[1])
        return (x, y)

    def __init__ (self, function, importance, anchor_point, constraints, min_dimensions, max_dimensions):
        self.function = function
        self.importance = importance
        self.anchor_point = anchor_point
        self.constraints = constraints
        self.min_dimensions = min_dimensions
        self.max_dimensions = max_dimensions

class Furniture(object):
    function = ''
    bounds = Bounds(0, 0, 0, 0)

    def __init__ (self, function, bounds):
        self.function = function
        self.bounds = bounds

class Layout(object):
    room = []
    furnitures = []

    def __init__ (self, room, furnitures):
        self.room = room
        self.furnitures = furnitures

FURNITURE_INFO = {
    'table': FurnitureInfo('table', 5, 'cc', [], (1, 2), (3, 4)),
    'chair': FurnitureInfo('chair', 2, 'cc', ['table'], (1, 1), (1, 1)),
    'oven': FurnitureInfo('oven', 3, 'cc', ['wall'], (1, 1), (2, 2)),
    'plant': FurnitureInfo('plant', 5, 'cc', ['wall'], (1, 1), (1, 1)),
}

def generate_furnitures (layout, furniture_infos):
    sorted_furniture_infos = deepcopy(furniture_infos)
    sorted_furniture_infos.sort(key = lambda info: info.importance)

    for furniture_info in sorted_furniture_infos:
        dimensions = furniture_info.generate_dimensions()
        bounds = Bounds(0, 0, dimensions[0], dimensions[1])
        location = place_furniture (layout, bounds, furniture_info.constraints)
        new_furniture = Furniture(furniture_info.function, move_bounds(bounds, location))
        layout.furnitures.append(new_furniture)

def place_furniture (layout, bounds, constraints):
    possible_locations = find_possible_locations(layout, bounds, constraints)
    if len(possible_locations) == 0:
        return

    location_scores = [(location, score_location(layout, location)) for location in possible_locations]
    best_option = max(location_scores, key=lambda option: option[1])
    return best_option[0]

def score_location (layout, location):
    wall_score_value = 1
    room_bounds = bounds_from_points(layout.room)
    closest_walls = [   (room_bounds.x1, location[1]),
                        (room_bounds.x2, location[1]),
                        (location[0], room_bounds.y1),
                        (location[0], room_bounds.y2) ]
    closest_wall = min(closest_walls, key = lambda wall: get_distance_score(location, wall, 0.8))
    wall_score_value = get_distance_score (location, closest_wall, 0.8)

    dist_product = 1
    for furniture in layout.furnitures:
        dist_product *= get_distance_score( (furniture.bounds.x1, furniture.bounds.y1), location )
    return dist_product * wall_score_value

def get_distance_score (point1, point2, dist_multiplier=1):
    dist = sqrt((point1[0] - point2[0])**2 + (point1[1] - point2[1])**2) * dist_multiplier
    a, b = 15, 2.6 # yay, magic constants
    dist_score = dist - a * sqrt(dist / b) # initially negative to discourage closeness, then increases with distance
    #dist_score = dist - 1
    return dist_score

def find_possible_locations (layout, bounds, constraints):
    empty_locations = []
    for point in layout.room:
        check_bounds = move_bounds(bounds, point)
        if check_empty(layout, check_bounds):
            empty_locations.append(point)

    constraint_locations = find_constraint_locations(layout, constraints)

    possible_locations = []
    for point in empty_locations:
        if point in constraint_locations:
            possible_locations.append(point)
    
    return possible_locations
            

def move_bounds (bounds, move_vector):
    return Bounds(  move_vector[0] + bounds.x1, 
                    move_vector[1] + bounds.y1, 
                    move_vector[0] + bounds.x2, 
                    move_vector[1] + bounds.y2 )

def check_empty (layout, check_bounds):
    furnitures_bounds = [furniture.bounds for furniture in layout.furnitures]
    room_bounds = bounds_from_points(layout.room)
    
    for check_point in check_bounds.get_points():
        if not room_bounds.contains(check_point):
            return False

    for furniture_bounds in furnitures_bounds:
        if check_bounds.intersect(furniture_bounds):
            return False
    
    return True

def find_constraint_locations (layout, constraints):
    constraint_points = []
    for furniture in layout.furnitures:
        if furniture in constraints:
            bounding_points = outline(furniture.bounds.get_points(), False)
            constraint_points.extend(bounding_points)
    if 'wall' in constraints:
        bounding_points = inline(layout.room)
        constraint_points.extend(bounding_points)
    
    if len(constraint_points) == 0:
        return layout.room
    else:
        return constraint_points

def draw_layout (layout):
    room_bounds = bounds_from_points(layout.room)
    furnitures = layout.furnitures
    furniture_map = {}
    for i in range(len(furnitures)):
        for point in furnitures[i].bounds.get_points():
            furniture_map[point[0], point[1]] = i

    for y in range(room_bounds.y1, room_bounds.y2 + 1):
        row = ''
        for x in range(room_bounds.x1, room_bounds.x2 + 1):
            point = (x, y)
            if point in furniture_map:
                item_color = COLORS[furniture_map[point] + 3]
                row += item_color + ' ' + Back.RESET
            else:
                row += COLORS[-1] + ' ' + Back.RESET
        print (row)

    draw_legend(layout)
            
def draw_legend (layout):
    for i in range(len(layout.furnitures)):
        furniture = layout.furnitures[i]
        color = COLORS_FG[i + 3]
        string = str(i) + ': ' + furniture.function
        print (color + string + Fore.RESET)

infos = [FURNITURE_INFO['table'], FURNITURE_INFO['chair'], FURNITURE_INFO['oven'], FURNITURE_INFO['plant']]
room = Bounds(0, 0, 30, 30).get_points()
initial_layout = Layout(room, [])
generate_furnitures(initial_layout, infos)

# print (initial_layout)
draw_layout(initial_layout)