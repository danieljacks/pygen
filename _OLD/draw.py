# from describe import World, Room
from colorama import init, Back, Fore
from house_generator import COLORS, COLORS_FG, get_wall_symbol
from classes import World
# init()


def draw_world (world, margin=2, show_color=True):
    print (world_representation(world, margin, show_color))

def world_representation (world, margin=2, show_color=True):
    bounds = world.get_bounds()

    output = ''

    output += upper_margin(margin, bounds.x2, show_color)    

    # rooms
    for y in range(bounds.y2 + 1):
        row = ' ' * margin
        if show_color: row = room_color(-1) + row + Back.RESET

        for x in range(bounds.x2 + 1):
            row += cell_string(world, (x, y), show_color)

        right_margin = ' ' * margin
        if show_color: right_margin = room_color(-1) + right_margin + Back.RESET
        row += right_margin
        
        output += row + '\n'

    output += upper_margin(margin, bounds.x2, show_color) 

    output += legend (world, show_color)

    return output

def legend (world, show_color):
    output = ''
    for i in range(len(world.rooms)):
        entry = str(i) + ': ' + repr(world.rooms[i].functions)
        if show_color:
            entry = room_color(world.rooms[i].room_id, color_type='fore') + entry + Fore.RESET
        output += entry + '\n'
    return output

def upper_margin (margin, room_width, show_color):
    rows = ''
    for i in range(margin):
        row = ' ' * 2 * margin + ' ' * room_width + ' '
        if show_color:
            row = room_color(-1) + row + Back.RESET
        rows += row + '\n'
    return rows

def cell_string (world, point, show_color):
    x, y = point[0], point[1]
    right_val   = world.get_cell((x + 1, y))
    up_val      = world.get_cell((x, y - 1))
    left_val    = world.get_cell((x - 1, y))
    down_val    = world.get_cell((x, y + 1))

    room_id = world.room_id_of(point)

    val = world.get_cell(point)

    if val == 0 or val==1 or val==2:
        entities = World.entities_at(point)
        player_entities = list(filter(lambda entity: entity.entity_type == 'Player', entities))
        char = '@' if len(player_entities) > 0 else ' '
    elif val == 1:
        char = get_wall_symbol(right_val, up_val, left_val, down_val)
    elif val == 2:
        char = 'X'
    else:
        char = 'asdkadhkjasdhkajs'
    
    if show_color:
        return room_color(room_id) + char + Back.RESET
    else:
        return char

def room_color (room_id, color_type='back'):
    if color_type == 'back' or color_type == 'Back': 
        return  COLORS[room_id]
    else:
        return COLORS_FG[room_id]