from house_generator import bounds_from_points, generate_house, outline
from constants import Material
from text_helpers import describe_list
from copy import deepcopy

# any thing which can be accessed by the text system is an entity
class Entity(object):
    # static
    __id_counter = 0
    __entity_dict = {}

    # instanced
    __id = -1

    def __init__ (self):
        self.__id = Entity.__id_counter
        Entity.__id_counter += 1
        Entity.__entity_dict[self.__id] = self

    # id should never be mutated
    @property
    def id (self):
        if self.__id == -1:
            raise Exception('id shouldnt be -1. You probably need to call super.__init__(self) in child class constructor function.')

        return self.__id

    @property
    def entity_type (self):
        return self.__class__.__name__

    @staticmethod
    def default_entity ():
        return Entity()

    @staticmethod
    def from_id (entity_id):
        return Entity.__entity_dict[entity_id]

    @staticmethod
    def entities ():
        return deepcopy(Entity.__entity_dict)

class World(object):
    cells = {} # {position: value}
    rooms = []

    def __init__ (self, cells, rooms):
        self.cells = cells
        self.rooms = rooms
        self.reset_offset()

    @staticmethod
    def entities_at (point):
        entities = Entity.entities()
        return list(filter(lambda entity: hasattr(entity, 'transform') and entity.transform.position == point, entities))

    def reset_offset (self):
        bounds = self.get_bounds()
        offset = bounds.x1, bounds.y1
        new_cells = {}
        for cell in self.cells:
            new_cell = (cell[0] - offset[0], cell[1] - offset[1])
            new_cells[new_cell] = self.cells[cell]
        self.cells = new_cells

        for room in self.rooms:
            room.move((-offset[0], -offset[1]))
            

    def room_by_id (self, room_id):
        for room in self.rooms:
            if room.room_id == room_id:
                return room    
    
    def get_cell(self, position):
        if position not in self.cells:
            return 0
        else:
            return self.cells[position].value

    def set_cell(self, position, val, material='wood'):
        if val == 0:
            if position in self.cells:
                self.cells.pop(position)
        else:
            if position in self.cells:
                self.cells[position].value = val
            else:
                self.cells[position] = Cell(val, material)

    def room_id_of (self, position):
        for room in self.rooms:
            for point in room.points:
                if point == position:
                    return self.rooms.index(room)
        return -1

    def room_of (self, position):
        return self.room_by_id(self.room_id_of(position))

    def get_points (self):
        points = []
        for cell in self.cells:
            points.append(cell)
        for room in self.rooms:
            for point in room.points:
                if point not in points: points.append(point)
        return points

    def get_bounds (self):
        return bounds_from_points(self.get_points())

class Cell(object):
    value = 0
    material = 'wood'

    def __init__ (self, value, material):
        self.value = value
        self.material = material

class Room(Entity):
    room_id = 0
    functions = []
    points = []
    doors = []

    def __init__ (self, room_id, functions, points, doors):
        super().__init__()
        self.room_id = room_id
        self.functions = functions
        self.points = points
        self.doors = doors 

    def get_bounds (self):
        return bounds_from_points(self.points)

    def move(self, delta):
        for i in range(len(self.points)):
                self.points[i] = (self.points[i][0] + delta[0], self.points[i][1] + delta[1])

        for door in self.doors:
            door.position = (door.position[0] + delta[0], door.position[1] + delta[1])

    def __str__ (self):
        if len(self.functions) == 1:
            return self.functions[0]
        return 'room which is ' + describe_list(self.functions)

class Door(Entity):
    position = (0, 0)
    material = Material.wood
    adjectives = []

    def __init__ (self, position, material, adjectives): 
        super().__init__()
        self.position = position
        self.material = material
        self.adjectives = adjectives

    def __str__ (self):
        return self.material.name + ' door'

class Transform(object):
    position = (0, 0)
    direction = (1, 0)

    def __init__ (self, position, direction):
        self.position = position
        self.direction = direction

class Player(Entity):
    transform = None
    memory = None

    def __init__ (self, transform, memory):
        super().__init__()
        self.transform = transform
        self.memory = memory

class Theme(object):
    materials = []

    def __init__ (self, materials):
        self.materials = materials