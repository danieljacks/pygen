from math import floor, ceil
from random import randint

def clamp(val, min_bound, max_bound):
    return max(min_bound, min(val, max_bound))

def snap(val, snap_type):
    if snap_type == 'ceil':
        return ceil(val)
    if snap_type == 'floor':
        return floor(val)
    if snap_type == 'random':
        return floor(val) if randint(0,1) == 1 else ceil(val)
    raise Exception('Unrecognised snap_type')

def manhattan_distance(x1, y1, x2, y2):
    return abs(x2 - x1) + abs(y2 - y1)

def geometric_median(points):
    return min(map(lambda p1:(p1,sum(map(lambda p2:manhattan_distance(p1[0], p1[1], p2[0], p2[1]),points))),points), key = lambda x:x[1])[0]