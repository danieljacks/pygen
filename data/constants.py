from colorama import init, Back, Fore


DIRECTIONS = [(-1,0),(1,0),(0,-1),(0,1)]
DIAGONALS = [(-1, -1), (-1, 1), (1, 1), (1, -1)]
COLORS_BACK = [Back.CYAN, Back.RED, Back.GREEN, Back.YELLOW, Back.BLUE, Back.MAGENTA, Back.GREEN, Back.YELLOW, Back.BLACK, Back.RED, Back.MAGENTA, Back.BLUE, Back.CYAN]
COLORS_FORE = [Fore.WHITE, Fore.RED, Fore.GREEN, Fore.YELLOW, Fore.BLUE, Fore.MAGENTA, Fore.GREEN, Fore.YELLOW, Fore.WHITE, Fore.RED, Fore.MAGENTA, Fore.BLUE, Fore.CYAN]
