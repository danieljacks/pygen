from random import randint, random
from copy import deepcopy, copy
from math import inf, exp, floor, ceil

from classes.grid import Grid
from classes.rect import Rect
from classes.region import Region
from data.constants import DIRECTIONS, DIAGONALS
from classes.position import Position

def merge_rooms(rooms):
    return sum(rooms, Region()).split()

# generates random connected rectangular rooms
def random_rooms(bounds, min_dim, max_dim, min_rooms, max_rooms, initial_point):
    rooms = [Region()]
    rooms_count = randint(min_rooms, max_rooms)

    for i in range(rooms_count):
        width = randint(min_dim, max_dim)
        height = randint(min_dim, max_dim)

        if (len(sum(rooms, Region()).positions) != 0):
            i = randint(0, len(sum(rooms, Region()).positions) - 1)
            pos = sum(rooms, Region()).positions[i]
        else:
            pos = initial_point

        room = Region(Rect.from_anchor(pos.x, pos.y, width, height, 'cc', 'random').positions)
        rooms.append(room)

    return merge_rooms(rooms)

# returns worst (highest) score for rooms
def test_rooms (rooms, expecting_two_rooms=True):
    room_values = []
    for room in rooms:
        inner_area = room.area
        # a, b = 8, 0.7 # old values
        a, b = 5, 0.2
        size_factor = a * exp(-b * inner_area) # large value for small numbers, small value for anything past 4ish
        ratio_factor = 3 * max(abs(1 - room.height / room.width), abs(1 - room.width / room.height))
        regularity_factor = 4 * abs(1 - room.area / room.bounds.area)
        room_values.append(ratio_factor + size_factor + regularity_factor)
    
    # if expecting_two_rooms:
    #     if len(room_values) == 1 or len(room_values) == 0:
    #         return inf # we dont want a double wall, we are expecting two rooms but got only 0 or 1
    score = sum(room_values) / len(room_values) - (len(room_values) * 2) # return worst value
    return score

# creates walls extending from concave corners
def walls_from_corners (rooms, max_room_score):
    divided_rooms = deepcopy(rooms)
    for room in rooms:
        concave_corners = room.concave_corners()
        for corner in concave_corners:
            divide_options = [{ 'rooms': [room], 'score': max_room_score }]
            for direction in DIRECTIONS:
                divided_rooms = sum(divided_rooms, Region()).remove_line(corner, direction).split()
                score = test_rooms(divided_rooms)
                divide_options.append({ 'rooms': divided_rooms, 'score': score })
            best_option = min(divide_options, key = lambda option: option['score'])['rooms']
            divided_rooms = best_option
    return sum(divided_rooms, Region()).split()

def split_room(room, max_room_score):
    bounds = room.bounds
    if bounds.height > bounds.width:
        direction = (1, 0)
    else:
        direction = (0, 1)

    perp = (1 - direction[0], 1 - direction[1])
    cut_proportion = 0.3 + random() * 0.4
    cut_start = Position(bounds.x1 + perp[0] * floor(bounds.width * cut_proportion), 
                bounds.y1 + perp[1] * floor(bounds.height * cut_proportion))

    divided_rooms = room.copy().remove_line(cut_start, direction).split()
    score = test_rooms(divided_rooms)
    if score > max_room_score or len(divided_rooms) <= 1:
        return [room], False
    else:
        return divided_rooms, True

def split_rooms(rooms, desired_rooms, max_room_score):
    output_rooms = deepcopy(rooms)
    rooms_to_add = []
    rooms_to_delete = []
    did_divide = True
    while did_divide == True:
        did_divide = False
        for room in output_rooms:
            if len(output_rooms) > desired_rooms: 
                break
            for _ in range(2):
                divided_rooms, successful = split_room(room, max_room_score)
                if successful:
                    did_divide = True
                    rooms_to_delete.append(room)
                    rooms_to_add.extend(divided_rooms)
                    break
        
        for room_to_delete in rooms_to_delete:
            output_rooms.remove(room_to_delete)
        for room_to_add in rooms_to_add:
            output_rooms.append(room_to_add)
        rooms_to_delete = []
        rooms_to_add = []

        output_rooms = merge_rooms(output_rooms)

        if len(output_rooms) > desired_rooms: break

    return output_rooms

# create a line that goes until it does not intersect any rooms
def line_intersecting(rooms, start_point, direction):
    all_rooms = sum(rooms, Region())
    bounds = sum(rooms, Region()).bounds
    did_add = False
    next_position = Position(start_point.x + direction[0], start_point.y + direction[1])
    line_points = []

    while bounds.contains(next_position) and (did_add == False or next_position in all_rooms.positions):
        if next_position in all_rooms.positions:
            did_add = True
            line_points.append(next_position)

        next_position = Position(next_position.x + direction[0], next_position.y + direction[1])
    
    return Region(line_points)

def rooms_intersecting(rooms, region):
    intersecting_rooms = []
    for position in region.positions:
        for room in rooms:
            if position in room.positions and room not in intersecting_rooms:
                intersecting_rooms.append(room)
    
    return intersecting_rooms

def hallway(rooms, max_room_score, hallway_length_per_width):
    new_rooms = deepcopy(rooms)
    hallway_options = []

    for room in rooms:
        for direction in DIRECTIONS:
            hallway_region = line_intersecting(rooms, room.center, direction)
            score = len(rooms_intersecting(rooms, hallway_region))
            hallway_options.append({ 'hallway': hallway_region, 'score': score })
    
    best_hallway = max(hallway_options, key = lambda option: option['score'])['hallway']

    # expand hallway
    hallway_width = (random() + 0.5) * (max(best_hallway.bounds.width, best_hallway.bounds.height) / hallway_length_per_width)
    best_hallway = best_hallway.expand(thickness=round(hallway_width))

    return best_hallway

# higher room score is a worse room (small or odd shape)
def generate_house(width, height, max_room_score = -1):
    bounds = Rect(0, 0, width, height)
    rooms = random_rooms(bounds, 4, 11, 2, 3, bounds.center('floor'))
    rooms = walls_from_corners(rooms, 10)
    rooms = split_rooms(rooms, 10, max_room_score)
    # hallway_room = hallway(rooms, max_room_score, 5)

    return rooms, Region()#, hallway_room