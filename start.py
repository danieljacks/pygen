# entry point
from random import seed
from gen.house import generate_house
from draw.ui import print_ui
from parse.commands import parse_command
from classes.world import World
from classes.room import Room
from colorama import init, Back, Fore


world = World([])

def initialize ():
    global world
    seed(12345)
    house_rooms, hallway_room = generate_house(30, 30)
    room_regions = house_rooms + [hallway_room]
    rooms = [Room(room_region, []) for room_region in room_regions]
    world = World(rooms)

initialize()

should_quit = False
while(not should_quit):
    print('-')
    print_ui(world)
    command = parse_command(input())
    if command == 'q':
        should_quit = True
    if command == 'r':
        initialize()
