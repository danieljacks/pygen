from copy import copy
from math import inf
from copy import deepcopy

from helpers.math import manhattan_distance, geometric_median
from classes.rect import Rect
from classes.position import Position
from data.constants import DIRECTIONS, DIAGONALS

# a grid is a collection of object
class Grid(object):
    positions_dict = {} # Position: True

    def __init__ (self, positions = [], val = None):
        self.positions_dict = {}
        for position in positions:
            self.add(position, val)

    @property
    def positions(self):
        return [pos for pos in self.positions_dict]

    @property
    def min_x(self):
        if len(self.positions) == 0: return 0
        return min(pos.x for pos in self.positions)

    @property
    def max_x(self):
        if len(self.positions) == 0: return 0
        return max(pos.x for pos in self.positions)

    @property
    def min_y(self):
        if len(self.positions) == 0: return 0
        return min(pos.y for pos in self.positions)

    @property
    def max_y(self):
        if len(self.positions) == 0: return 0
        return max(pos.y for pos in self.positions)

    @property
    def width(self):
        return self.max_x - self.min_x + 1

    @property
    def height(self):
        return self.max_y - self.min_y + 1

    @property
    def bounds(self):
        return Rect(self.min_x, self.min_y, self.max_x, self.max_y)

    @property
    def area(self):
        return len(self.positions)

    @property
    def center(self):
        points = [[p.x, p.y] for p in self.positions]
        median = geometric_median(points)
        return Position(median[0], median[1])

    def contains(self, pos):
        return pos in self.positions_dict

    def delete(self, pos):
        self.positions_dict.pop(pos, None)

    # item is added to whatever is already there
    def add(self, pos, val):
        if val != None:
            if not self.contains(pos):
                self.positions_dict[pos] = val
            else:
                self.positions_dict[pos] += val

    def get(self, pos):
        if pos not in self.positions:
            return None
        else:
            return self.positions_dict[pos]

    def intersects(self, other_region):
        for pos in self.positions:
            if other_region.contains(pos):
                return True
        return False

    def fill(self, bounds, val, outline = False):
        positions = []
        if outline == True:
            positions = bounds.outline
        else:
            positions = bounds.positions

        for pos in positions:
            self.add(pos, val)

    # the union of the two grids. Objects are added if they share a position
    def __add__(self, other):
        if isinstance(other, int):
            return self.copy()
        result = self.copy()
        for other_pos in other.positions:
            result.add(other_pos, other.positions_dict[other_pos])
        return result

    def __radd__(self, other):
        if isinstance(other, int):
            return self.copy()
        result = self.copy()
        for other_pos in other.positions:
            result.add(other_pos, other.positions_dict[other_pos])
        return result

    def __eq__(self, other) -> bool:
        self_positions = self.positions
        other_positions = other.positions
        if len(self_positions) != len(other_positions):
            return False
        for pos in self_positions:
            if pos not in other_positions or self.get(pos) != other.get(pos):
                return False
        return True

    def outline(self, include_diagonals=True, thickness=1):
        all_positions = copy(self.positions)
        outline_positions = []
        for _ in range(thickness):
            expanded_positions = []
            for position in all_positions:
                direction_list = DIRECTIONS + DIAGONALS if include_diagonals else DIRECTIONS
                for direction in direction_list:
                    new_position = Position(direction[0] + position.x, direction[1] + position.y)
                    if new_position not in expanded_positions:
                        expanded_positions.append(new_position)
            
            for position in expanded_positions:
                if position not in all_positions:
                    outline_positions.append(position)
                    all_positions.append(position)

        return outline_positions

    def inline(self, include_diagonals=True):
        inline_positions = []
        for position in self.positions:
            directions_list = DIRECTIONS + DIAGONALS if include_diagonals else DIRECTIONS
            outside_neighbours = 0
            for direction in directions_list:
                neighbour = Position(position.x + direction[0], position.y + direction[1])
                if neighbour not in self.positions:
                    outside_neighbours += 1
            if outside_neighbours >= 1:
                inline_positions.append(position)
        return inline_positions

    def connected_section(self, starting_position, include_diagonals=False, bounds=Rect.infinite()):
        if starting_position not in self.positions_dict:
            raise Exception('Starting position must be in the region')

        filled_positions = [starting_position]
        q = []
        q.append(starting_position)
        while len(q) > 0:
            node = q.pop()
            directions_list = DIRECTIONS + DIAGONALS if include_diagonals else DIRECTIONS
            for direction in directions_list:
                new_node = Position(node.x + direction[0], node.y + direction[1])
                if new_node in self.positions and new_node not in filled_positions and bounds.contains(new_node):
                    filled_positions.append(new_node)
                    q.append(new_node)

        new_grid = __class__()
        for position in filled_positions:
            new_grid.add(position, self.get(position))
        return new_grid

    def concave_corners (self):
        concave_points = []
        for point in self.outline():
            inside_neighbours = 0
            for direction in DIRECTIONS:
                neighbour = Position(point.x + direction[0], point.y + direction[1])
                if neighbour in self.positions:
                    inside_neighbours += 1
            if inside_neighbours == 2 or inside_neighbours == 3:
                concave_points.append(point)
        return concave_points

    # split any unconnected sections of this region into new regions and return the new subregions
    def split(self):
        subregions = []
        unprocessed_positions = copy(self.positions)
        while len(unprocessed_positions) > 0:
            subregion = self.connected_section(unprocessed_positions[0])
            for subregion_position in subregion.positions:
                unprocessed_positions.remove(subregion_position)
            subregions.append(subregion)
        return subregions

    def remove_line(self, starting_position, direction):
        bounds = self.bounds
        if starting_position in self.positions:
            self.delete(starting_position)
        did_delete = False
        next_position = Position(starting_position.x + direction[0], starting_position.y + direction[1])

        while bounds.contains(next_position) and (did_delete == False or next_position in self.positions):
            if next_position in self.positions:
                did_delete = True
            self.delete(next_position)
            next_position = Position(next_position.x + direction[0], next_position.y + direction[1])

        return self

    def copy(self):
        new_grid = self.__class__()
        for pos in self.positions:
            new_grid.add(pos, self.get(pos))
        return new_grid