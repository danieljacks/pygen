#material
from constants import Material
from classes import Room, Door, Transform, Player, Entity, Theme
from random import sample, choice
from math import ceil

def generate_theme (material_variance_factor=0.5, material_count = -1):
    if material_count == -1:
        material_count = ceil(material_variance_factor * len(Material))
    materials = sample(list(Material), material_count)
    return Theme(materials)

def decorate_room (room):
    theme = generate_theme (material_count=2)
    set_door_materials (room, theme)

def set_door_materials (room, theme):
    for door in room.doors:
        door.material = choice(theme.materials)
