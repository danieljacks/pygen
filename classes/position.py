# a position is an immutable x and y coordinate
class Position(object):
    x = 0
    y = 0

    def __init__ (self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other): 
        if not isinstance(other, Position):
            # don't attempt to compare against unrelated types
            return NotImplemented

        return self.x == other.x and self.y == other.y

    def __hash__(self):
        # necessary for instances to behave sanely in dicts and sets.
        return hash((self.x, self.y))

    def __str__(self) -> str:
        return f'({self.x}, {self.y})'

    def __repr__(self) -> str:
        return f'({self.x}, {self.y})'