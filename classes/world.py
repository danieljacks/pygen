from classes.grid import Grid

class World(object):
    # static
    __entities = {}

    # instance
    grid = Grid()
    rooms = []

    @staticmethod
    def register_entity(entity):
        World.__entities[entity.id] = entity
    
    @staticmethod
    def deregister_entity(entity):
        del World.__entities[entity.id]

    def __init__ (self, rooms):
        self.grid = Grid()
        self.rooms = rooms
    
    def get_rooms_containing(self, pos):
        return [room for room in self.rooms if room.region.contains(pos)]