from copy import copy
from math import inf

from helpers.math import manhattan_distance, geometric_median
from classes.rect import Rect
from classes.position import Position
from data.constants import DIRECTIONS, DIAGONALS
from classes.grid import Grid

# a region is a collection of points
class Region(Grid):
    def __init__(self, positions = []):
        super().__init__(positions, 1)

    def add(self, pos, val=1):
        if not self.contains(pos):
            super().add(pos, val)

    def fill(self, bounds, outline=False):
        super().fill(bounds, 1, outline)

    def expand(self, include_diagonals=True, thickness=1):
        outline_points = self.outline(include_diagonals, thickness)
        return self + Region(outline_points)