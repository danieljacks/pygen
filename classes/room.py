from classes.entity import Entity
from classes.region import Region

class Room(Entity):
    functions = []
    region = Region()

    def __init__ (self, region, functions):
        super().__init__()
        self.region = region
        self.functions = functions