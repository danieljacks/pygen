from enum import Enum
from random import choice

class Material(Enum):
    wood = 0
    stone = 1
    marble = 2
    plaster = 3

    @staticmethod
    def random():
        return choice(list(Material))