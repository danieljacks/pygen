# from classes.position import Position
# from classes.rect import Rect

# # a grid is a grid of cells with each cell holding an array of entities
# class Grid(object):
#     cells = {} # Position: (Entities)

#     def __init__ (self):
#         self.cells = {}

#     def is_empty(self, pos):
#         return pos not in self.cells

#     def get(self, pos):
#         if pos in self.cells:
#             return self.cells[pos]
#         else:
#             return ()

#     def delete_all(self, pos):
#         if not self.is_empty(pos):
#             del self.cells[pos]

#     def delete(self, pos, entity_id):
#         if not self.is_empty(pos):
#             self.cells[pos] = (entity for entity in self.cells[pos] if entity.id != entity_id)
#             if self.cells[pos] == ():
#                 del self.cells[pos]

#     def add(self, pos, val):
#         if not self.is_empty(pos):
#             self.cells[pos] = self.cells[pos] + (val)
#         else:
#             self.cells[pos] = (val)

#     def replace(self, pos, val):
#         self.delete_all(pos)
#         self.add(pos, val)

#     @property
#     def width(self):
#         return max(pos.x for pos in self.cells) - min(pos.x for pos in self.cells)

#     @property
#     def height(self):
#         return max(pos.y for pos in self.cells) - min(pos.y for pos in self.cells)

#     @property
#     def bounds(self):
#         return Rect(0, 0, self.width, self.height)

#     def entities(self):
#         return sum([entities for pos, entities in self.cells], [])

#     def fill(self, bounds, val, outline = False):
#         positions = []
#         if outline == True:
#             positions = bounds.outline
#         else:
#             positions = bounds.positions

#         for pos in positions:
#             self.add(pos, val.copy())