# encoding: utf-8

from math import floor, ceil, inf, exp, isinf
from random import randint, seed, random
import queue
import enum
from time import time
from colorama import init, Back, Fore
from functools import reduce
from room_names import get_room_types
from copy import deepcopy
# init()

DIRECTIONS = [(-1,0),(1,0),(0,-1),(0,1)]
DIAGONALS = [(-1, -1), (-1, 1), (1, 1), (1, -1)]
COLORS = [Back.BLACK, Back.RED, Back.GREEN, Back.YELLOW, Back.BLUE, Back.MAGENTA, Back.GREEN, Back.YELLOW, Back.BLACK, Back.RED, Back.MAGENTA, Back.BLUE, Back.CYAN]
COLORS_FG = [Fore.WHITE, Fore.RED, Fore.GREEN, Fore.YELLOW, Fore.BLUE, Fore.MAGENTA, Fore.GREEN, Fore.YELLOW, Fore.WHITE, Fore.RED, Fore.MAGENTA, Fore.BLUE, Fore.CYAN]

# note: some functions modify the input arrays directly. This may eventually cause some bugs, but I dont want to start doing deepcopies if they are not currently necessary
# done: make split rooms recursive (using a while loop and a variable to check if all rooms are too small to be further subdivided) so we can split very large rooms. Also implement minimum size checking for rooms
# done: make the split room function use the same test score method as the walls from corners function, for more consistency
# todo: make door generation
# pass: make infinite colors
# todo: add cleaning to remove double walls (like seed 1555906128)
# done: make room splitting random (i.e. not always half) and make walls move around after hallways are placed so that the rooms are not symmetrical on both sides of the hallway...
# TODO: delete empty rooms at end of generation (they can become empty if hallways take all their space...)
# TODO: fixe rooms with no door pathway when a double wall extends the whole length of a hallway, as in seed 1556631288
# TODO: fix max_score setting in house gen. Pretty sure it doesnt do anything currently

def clamp (val, min_bound, max_bound):
    return max(min_bound, min(val, max_bound))

class AnchorPoint(enum.Enum):
    left_top = 'lt'
    left_centre = 'lc'
    left_bot = 'lb'
    centre_top = 'ct'
    centre_centre = 'cc'
    centre_bot = 'cb'
    right_top = 'rt'
    right_centre = 'rc'
    right_bot = 'rb'

class Bounds(object):
    x1, y1, x2, y2 = 0, 0, 0, 0

    @property
    def width (self):
        return self.x2 - self.x1 + 1
    @property
    def height (self):
        return self.y2 - self.y1 + 1
    
    @property
    def area (self):
        return self.height * self.width

    @property
    def vertices (self):
        return [(self.x1,self.y1), (self.x1, self.y2), (self.x2,self.y2), (self.x2,self.y1)]

    def contains (self, point):
        return point[0] == clamp(point[0], self.x1, self.x2) and point[1] == clamp(point[1], self.y1, self.y2)

    def intersect (self, other_bounds):
        for other_vertex in other_bounds.vertices: # check for other vertices in our bounds
            if self.contains(other_vertex):
                return True
        for vertex in self.vertices:
            if other_bounds.contains(vertex): # check for our vertices in other bounds
                return True
        return False

    def get_points (self):
        if isinf(self.x1) or isinf(self.y1) or isinf(self.x2) or isinf(self.y2):
            return None
        points = []
        for y in range(self.y1, self.y2 + 1):
            for x in range(self.x1, self.x2 + 1):
                points.append((x, y))
        return points

    def __init__ (self, x1, y1, x2, y2):
        if x1 > x2:
            x1, x2 = x2, x1
        if y1 > y2:
            y1, y2 = y2, y1

        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        
def bounds_from_anchor (width, height, x, y, anchor_type, snap_function = lambda val: floor(val) if randint(0,1) == 1 else ceil(val) ):
    if isinstance(anchor_type, AnchorPoint):
        anchor_type = anchor_type.value
    
    if anchor_type[0] == 'l':
        x1 = x
    elif anchor_type[0] == 'c':
        x1 = x - snap_function((width - 1)/2)
    elif anchor_type[0] == 'r':
        x1 = x - width

    if anchor_type[1] == 't':
        y1 = y
    elif anchor_type[1] == 'c':
        y1 = y - snap_function((height - 1)/2)
    elif anchor_type[1] == 'b':
        y1 = y - height

    x2 = x1 + width
    y2 = y1 + height

    return Bounds(x1, y1, x2, y2)

def bounds_from_point (point):
    return Bounds (point[0], point[1], point[0], point[1])

# cells are first pending (pendong cells are completely overwritten for each new generator), then goes to cells array (which is added to with new generators)
class Grid(object):
    # grid of cell values only
    _cells = []
    # list of coordinates and values, (x, y, val) for not applied generators
    _pending_cells = []
    _rooms = []
    _initial_point = (0, 0)
    _hallways = []

    def __init__ (self, width, height):
        self._cells = []
        self._pending_cells = []
        self._rooms = []
        self._initial_point = (0, 0)
        # empty grid
        for i in range(height):
            self._cells.append(width * [0])

    def get_cell(self, x, y):
        if (self.in_bounds(x, y) == False):
            return 0
        else:
            val = self._cells[y][x]
            return val

    def set_cell(self, x, y, val):
        if (self.in_bounds(x, y) == False):
            return
        else:
            self._cells[y][x] = val

    def get_room (self, x, y):
        for room in self._rooms:
            # if len(room) == 0:
            #     return -1
            for point in room:
                if point[0] == x and point[1] == y:
                    return self._rooms.index(room)
        return -1

    def replace_pending_cells (self, new_pending_cells):
        for new_cell in new_pending_cells:
            for old_cell in self._pending_cells:
                if new_cell[0] == old_cell[0] and new_cell[1] == old_cell[1]:
                    self._pending_cells.remove(old_cell)
                    self._pending_cells.append(new_cell)

    def pending_cells_with_values (self, required_vals):
        return list(filter(lambda cell: cell[2] in required_vals, self._pending_cells))

    @property
    def width(self):
        return len(self._cells[0])

    @property
    def height(self):
        return len(self._cells)

    @property
    def bounds(self):
        return Bounds(0, 0, self.width - 1, self.height - 1)

    def in_bounds (self, x, y):
        return x == clamp(x, 0, self.width - 1) and y == clamp(y, 0, self.height - 1)

    def get_cells (self):
        cells = {}
        for y in range(0, self.height):
            for x in range(0, self.width):
                if self.get_cell(x, y) != 0:
                    cells[(x, y)] = self.get_cell(x, y)
        return cells

    def rectangle (self, bounds, filled = False):

        bounds.x1, bounds.y1 = clamp(bounds.x1, 0, self.width), clamp(bounds.y1, 0, self.height)
        bounds.x2, bounds.y2 = clamp(bounds.x2, 0, self.width), clamp(bounds.y2, 0, self.height)

        points = rectangle(bounds, filled)
        cells = points_to_cells(points, 1)
        self._pending_cells = cells

    def random_rooms (self, min_dim, max_dim, min_rooms, max_rooms, initial_point):
        self._initial_point = initial_point
        rooms = random_rooms(min_dim, max_dim, min_rooms, max_rooms, initial_point)
        points = [point for room in rooms for point in room]
        points = list(set(points)) # remove duplicates
        cells = points_to_cells(points, 1)
        self._pending_cells = cells

    def outline (self):
        points = cells_to_points(self._pending_cells)
        outline_points = outline(points)
        self._pending_cells = points_to_cells(outline_points, 1)

    def walls_from_corners (self):
        edge_points = cells_to_points (self.pending_cells_with_values([1]))
        concave_points = concave_corners(edge_points, self._initial_point)
        if len(concave_points) == 0:
            return
        self.replace_pending_cells(points_to_cells(concave_points, 1))
        walls = walls_from_corners(edge_points, concave_points, self._initial_point)
        self._pending_cells.extend(points_to_cells(walls, 1))

    def split_rooms (self, desired_rooms, max_room_score):
        cells = self.pending_cells_with_values([1, 2])
        walls = cells_to_points(cells)
        rooms = get_rooms(walls, self.bounds)
        new_walls, new_rooms = split_rooms(walls, rooms, desired_rooms, max_room_score)
        self._rooms = new_rooms
        self._pending_cells.extend(points_to_cells(new_walls, 1))

    def hallway (self):
        old_edge_points = cells_to_points(self.pending_cells_with_values([1]))
        edge_points, rooms, hallway_room = hallway(old_edge_points, self._rooms)
        self._hallways.append(hallway_room)
        self._pending_cells = points_to_cells(edge_points, 1)
        self._rooms = rooms

    def doors (self):
        edge_points = cells_to_points(self.pending_cells_with_values([1]))
        rooms = self._rooms
        hallway_rooms = self._hallways
        door_points = doors(edge_points, rooms, hallway_rooms)
        self.replace_pending_cells(points_to_cells(door_points, 2))

    def apply_pending (self):
        for point in self._pending_cells:
            self.set_cell (point[0], point[1], point[2])
        self._pending_cells = []

def doors (edge_points, rooms, hallway_rooms):
    # check if room is the hallway!
    door_points = []
    connected_rooms = hallway_rooms[:]
    rooms_with_path_value = []
    snap_function = lambda val: floor(val) if randint(0,1) == 1 else ceil(val) 
    
    not_hallway_rooms = deepcopy(rooms)
    for hallway_room in hallway_rooms: not_hallway_rooms.remove(hallway_room)

    for current_room in hallway_rooms + not_hallway_rooms:
        room_intersections = {}
        current_outline = outline(current_room)
        for point in current_outline:
            for d in DIRECTIONS:
                new_point = (point[0] + d[0], point[1] + d[1])
                opposite_point = (point[0] - d[0], point[1] - d[1])
                for room in rooms:
                    if new_point in room and opposite_point in current_room:
                        if repr(room) not in room_intersections: room_intersections[repr(room)] = []
                        room_intersections[repr(room)].append(point)
    
        for room in room_intersections:
            if room in connected_rooms:
                continue
            points = room_intersections[room]
            avg_x = snap_function( sum([point[0] for point in points]) / len(points) )
            avg_y = snap_function( sum([point[1] for point in points]) / len(points) )
        
            door_points.append ((avg_x, avg_y))
            connected_rooms.append(room)

    return door_points


def hallway (edge_points, rooms):
    # scan each row and count how many rooms a hallway at that location would touch. 
    # do the same for columns. 
    # add the hallway with the most rooms
    
    rooms_points = [point for room in rooms for point in room]
    rooms_bounds = bounds_from_points(rooms_points)
    hallway_options = []

    x1, y1 = rooms_bounds.x1, rooms_bounds.y1

    # horizontal hallway options
    for y in range(y1, y1 + rooms_bounds.height):
        hallway_room = []
        for x in range(x1, x1 + rooms_bounds.width):
            hallway_room.append((x, y))
        score = count_rooms_for_hallway (rooms, hallway_room)
        hallway_options.append([hallway_room, score])

    # vertical hallways options
    for x in range(x1, x1 + rooms_bounds.width):
        hallway_room = []
        for y in range(y1, y1 + rooms_bounds.height):
            hallway_room.append((x, y))
        score = count_rooms_for_hallway (rooms, hallway_room)
        hallway_options.append([hallway_room, score])

    best_option = max(hallway_options, key = lambda option: option[1])
    
    # clear hallway
    for hallway_point in best_option[0]:
        if hallway_point in edge_points: edge_points.remove(hallway_point)
        
        for i in range(len(rooms)):
            if hallway_point in rooms[i]: rooms[i].remove(hallway_point)

    # add hallway walls
    hallway_outline = outline(best_option[0])
    for outline_point in hallway_outline:
        for i in range(len(rooms)):
            if outline_point in rooms[i]: 
                rooms[i].remove(outline_point)
        if outline_point not in edge_points: 
            edge_points.append(outline_point)

    non_empty_rooms = []
    for room in rooms:
        if len(room) > 0:
            non_empty_rooms.append(room)
    
    return edge_points, non_empty_rooms + [best_option[0]], best_option[0]

def count_rooms_for_hallway (rooms, hallway_room):
    counted_rooms = []
    for hallway_point in hallway_room:
        for d in DIRECTIONS:
            new_point = (hallway_point[0] + d[0], hallway_point[1] + d[1])
            for room in rooms:
                if new_point in room and room not in counted_rooms and new_point not in hallway_room:
                    counted_rooms.append(room)
    
    return len(counted_rooms)

def points_to_cells (points, cell_value):
    if len(points) == 0:
        return []
    if not isinstance(points[0], tuple): # i.e. only a single point
        return (points[0], points[1], cell_value)
    
    cells = []
    for point in points:
        cells.append((point[0], point[1], cell_value))
    return cells


def cells_to_points (cells):
    if not isinstance(cells[0], tuple): # i.e. only a single point
        return (cells[0], cells[1])
    
    points = []
    for cell in cells:
        points.append((cell[0], cell[1]))
    return points

def bounds_from_points (points):
    x1, y1 = inf, inf
    x2, y2 = 0, 0

    for point in points:
        x1, y1 = min(x1, point[0]), min(y1, point[1])
        x2, y2 = max(x2, point[0]), max(y2, point[1])

    if x1 == inf or y1 == inf:
        x1 = y1 = 0

    return Bounds(x1, y1, x2, y2)

def print_grid(grid, show_color, room_types):
    print('+' + '-' * (grid.width) +'+')
    for y in range(grid.height):
        row = '|'
        for x in range(grid.width):
            color = COLORS[grid.get_room(x, y)] if show_color else None
            row += get_symbol(grid, x, y, color)
        row += '|'
        print(row)
    print('+' + '-' * (grid.width) + '+')
    print_room_legend (grid, room_types, show_color)

def get_symbol(grid, x, y, color = Back.RESET):
    val = grid.get_cell(x, y)

    char = ''
    if val == 1 :
        right_val   = grid.get_cell(x + 1, y)
        up_val      = grid.get_cell(x, y - 1)
        left_val    = grid.get_cell(x - 1, y)
        down_val    = grid.get_cell(x, y + 1)
        char = get_wall_symbol(right_val, up_val, left_val, down_val)
    elif val == 0:
        char = ' '
    elif val == 2:
        char = 'o'
    elif val == 4:
        char = 'x'
    else:
        char = 'asdasdasdasdasdasdafsdagaag'

    return (color + char + Back.RESET) if color != None else char

def get_wall_symbol (right_val, up_val, left_val, down_val):
    val_list = [right_val, up_val, left_val, down_val]
    for i in range(len(val_list)):
        if val_list[i] != 0 and val_list[i] != 1:
            val_list[i] = 1
    val_lookup = {
        '[0, 1, 0, 1]': '│',
        '[1, 0, 1, 0]': '─',
        '[1, 1, 0, 0]': '└',
        '[1, 0, 0, 1]': '┌',
        '[0, 0, 1, 1]': '┐',
        '[0, 1, 1, 0]': '┘',
        '[0, 1, 1, 1]': '┤',
        '[1, 1, 1, 0]': '┴',
        '[1, 0, 1, 1]': '┬',
        '[1, 1, 0, 1]': '├',
        '[1, 1, 1, 1]': '┼',
        '[0, 0, 0, 0]': 'O',
        '[1, 0, 0, 0]': '╶',
        '[0, 1, 0, 0]': '╵',
        '[0, 0, 1, 0]': '╴',
        '[0, 0, 0, 1]': '╷',
    }
    return val_lookup[repr(val_list)]

def print_room_legend (grid, room_types, show_color):
    # this is getting very hacky...
    legend = []
    room_scores = []
    rooms = grid._rooms

    for room in rooms:
        if room not in grid._hallways:
            room_scores.append((room, test_rooms([room], False)))
    room_scores.sort(key = lambda room_score: room_score[1])

    for i in range(len(rooms)):
        if rooms[i] not in grid._hallways:
            index_in_score_list = 0
            for j in range(len(room_scores)):
                if room_scores[j][0] == rooms[i]:
                    index_in_score_list = j
            room_type = room_types[index_in_score_list]
        else:
            room_type = ['hallway']
        color = COLORS_FG[i]

        entry = str(i) + ': ' + repr(room_type)
        entry = color + entry + Fore.RESET if show_color else entry
        legend.append((entry, color))
    for entry in legend:
        print(entry[0])

def flood_fill (edge_points, starting_point):
    return flood_fill_bounds(edge_points, starting_point, Bounds(-inf, -inf, inf, inf))

def flood_fill_bounds (edge_points, starting_point, bounds):
    if starting_point in edge_points:
        return
    filled_points = []
    filled_points.append(starting_point)
    q = []
    q.append (starting_point)
    while len(q) > 0:
        node = q.pop()
        for direction in DIRECTIONS:
            new_node = (node[0] + direction[0], node[1] + direction[1])
            if new_node not in edge_points and new_node not in filled_points and bounds.contains(new_node):
                filled_points.append(new_node)
                q.append(new_node)
    return filled_points

# assumes the edge has no gaps
def concave_corners (edge_points, point_inside_polygon):
    inside_points = flood_fill(edge_points, point_inside_polygon)
    concave_points = []
    for point in edge_points:
        inside_neighbours = 0
        for direction in DIRECTIONS:
            neighbour = (point[0] + direction[0], point[1] + direction[1])
            if neighbour in inside_points:
                inside_neighbours += 1
        if inside_neighbours >= 2:
            concave_points.append(point)
    return concave_points

def walls_from_corners (edge_points, concave_points, point_inside_room):
    # dont make a wall if it will result in no room on one side (i.e. 2 cell thick wall)
    # 2n+4 options: make a wall left, right, up, down, or make an l wall to another point, either vertical then horizontal, or horizontal then vertical
    # keep functions for each of the 6 options in a list, with the width/height ratios of the 2 rooms they will create. Then choose the one with the ratios closest to one (function which does this for 2 inputs?)
    inside_points = flood_fill(edge_points, point_inside_room)
    none_option = [[], 2] # so if all the scores are larger than this, nothing will be chosen instead
    return_walls = []
    for concave_point in concave_points:
        wall_options = [none_option]
        for direction in DIRECTIONS:
            new_point = (concave_point[0] + direction[0], concave_point[1] + direction[1])
            if new_point in inside_points:
                walls = wall_until_edge(edge_points + return_walls, concave_point, direction)
                rooms = rooms_from_wall(edge_points + return_walls, walls)
                score = test_rooms(rooms) # want closest ratio to 1
                wall_options.append([walls, score])

        # for concave_point2 in concave_points:
        #     if concave_point2 != concave_point:
        #         for vert_first in [True, False]:
        #             walls = wall_between_points(concave_point, concave_point2, vert_first)

        #             wall_outside = False
        #             for wall in walls:
        #                 if wall not in inside_points:
        #                     wall_outside = True
        #             if wall_outside:
        #                 continue

        #             score = test_wall(edge_points + return_walls, walls)
        #             wall_options.append([walls, score])

        best_option = min(wall_options, key = lambda option: option[1])[0]
        return_walls.extend(best_option)
    
    return return_walls

def split_room (selected_room, rooms, edge_points):
    cut_rooms = []
    bounds = bounds_from_points(selected_room)
    if bounds.height > bounds.width:
        # cut horizontally
        d = (1, 0)
    else:
        # cut vertically
        d = (0, 1)
    
    perp = (1 - d[0], 1 - d[1])
    cut_proportion = 0.3 + random() * 0.4
    cut_start = (bounds.x1 + perp[0] * floor(bounds.width * cut_proportion) - 1, 
                 bounds.y1 + perp[1] * floor(bounds.height * cut_proportion) - 1 )# -1 because the room points do not include the wall, so we have to extend the bounds by one to include the walls
    cut_walls = wall_until_edge(edge_points, cut_start, d)
    if len(cut_walls) == 0:
        return None

    for i in range(len(cut_walls)):
        point_in_room1 = (cut_walls[i][0] - perp[0], cut_walls[i][1] - perp[1])
        point_in_room2 = (cut_walls[i][0] + perp[0], cut_walls[i][1] + perp[1])

        room_points = [point for room in rooms for point in room]
        for edge in edge_points:
            if edge in room_points:
                print('YESS')
        if point_in_room1 in room_points and point_in_room2 in room_points:
            cut_rooms.append(flood_fill(edge_points + cut_walls, point_in_room1))
            cut_rooms.append(flood_fill(edge_points + cut_walls, point_in_room2))
            return cut_walls, cut_rooms
    return None

def split_rooms (edge_points, rooms, desired_rooms, max_room_score):
    sorted_rooms = rooms
    rooms_to_remove = []
    new_walls = []
    split_some_rooms = True
    room_split_tries = 10
    while split_some_rooms == True:
        sorted_rooms = sorted(sorted_rooms, key = lambda room: bounds_from_points(room).area, reverse = True)
        split_some_rooms = False
        for selected_room in sorted_rooms:
            if len(sorted_rooms) - len(rooms_to_remove) >= desired_rooms:
                break

            for i in range(room_split_tries): # try a few times because randomness of cut position means different scores

                val = split_room(selected_room, sorted_rooms, edge_points + new_walls)
                if val == None:
                    continue

                cut_walls, cut_rooms = val
                score = test_rooms(cut_rooms)
                if score > max_room_score:
                    continue
                else:
                    break
            if val != None:
                sorted_rooms.extend(cut_rooms)
                new_walls.extend(cut_walls)
                rooms_to_remove.append(selected_room)
                split_some_rooms = True
                break # we want to update the sorted rooms every time anything changes, so there are no walls on top of rooms (this breaks the flood fill algorithm)

        for room in rooms_to_remove:
            sorted_rooms.remove(room)
        rooms_to_remove = []
    
    return_rooms = []
    for room in sorted_rooms:
        if len(room) > 0:
            return_rooms.append(room)
    
    return new_walls, return_rooms

def rooms_from_wall (edge_points, wall_points):
    rooms = []
    for wall in wall_points:
        for d in DIRECTIONS:
            new_point = (wall[0] + d[0], wall[1] + d[1])
            if new_point not in edge_points and new_point not in wall_points and new_point not in [point for room in rooms for point in room]:
                room = flood_fill(edge_points + wall_points, new_point)
                rooms.append(room)
    return rooms

# returns worst (highest) score for rooms
def test_rooms (rooms, expecting_two_rooms=True):
    room_values = []
    for room in rooms:
        room_bounds = bounds_from_points(room)
        inner_area = room_bounds.area
        # a, b = 8, 0.7 # old values
        a, b = 5, 0.2
        size_factor = a * exp(-b * inner_area) # large value for small numbers, small value for anything past 4ish
        ratio_factor = max(abs(1 - room_bounds.height / room_bounds.width), abs(1 - room_bounds.width / room_bounds.height))
        room_values.append(ratio_factor + size_factor)
    
    if expecting_two_rooms:
        if len(room_values) == 1 or len(room_values) == 0:
            return inf # we dont want a double wall, we are expecting two rooms but got only 0 or 1
    return max(room_values) # return worst value
    
def get_rooms (edge_points, bounds):
    extended_bounds = Bounds(bounds.x1, bounds.y1, bounds.x2 + 1, bounds.y2 + 1)
    rooms = []
    for wall in edge_points:
        for d in DIRECTIONS:
            new_point = (wall[0] + d[0], wall[1] + d[1])
            room_points = [point for room in rooms for point in room]
            if new_point not in edge_points and new_point not in room_points:
                new_room = flood_fill_bounds(edge_points, new_point, extended_bounds)
                rooms.append(new_room)
    for room in rooms:
        for point in room:
            if not bounds.contains(point):
                rooms.remove(room)
                return rooms
    return rooms

def wall_until_edge (edge_points, start_point, direction):
    wall_points = []
    next_point = (start_point[0] + direction[0], start_point[1] + direction[1])
    while next_point not in edge_points:
        wall_points.append(next_point)
        next_point = (next_point[0] + direction[0], next_point[1] + direction[1])
    return wall_points

def wall_between_points (start_point, end_point, vert_first):
    wall_points = []
    bounds = Bounds(start_point[0], start_point[1], end_point[0], end_point[1])
    rect_points = rectangle(bounds)
    if vert_first:
        wall_points = list(filter(lambda point: point[1] != start_point[1] and point[0] != end_point[0], rect_points)) # choose the 2 walls we want
    else:
        wall_points = list(filter(lambda point: point[0] != start_point[0] and point[1] != end_point[1], rect_points))
    return wall_points

def rectangle (bounds, filled = False):
    x1, y1, x2, y2 = bounds.x1, bounds.y1, bounds.x2, bounds.y2
    points = []

    # frame, top and bottom
    for x in range(x2 - x1 + 1):
        points.append((x1+x, y1+0))
        points.append((x1+x, y2))
    # frame, sides
    for y in range(y2 - y1 + 1):
        points.append((x1, y1+y))
        points.append((x2, y1+y))  
    
    if filled == False:
        return points

    # fill
    for y in range(y2 - y1 + 1):
        for x in range(x2 - x1 + 1):
            points.append((x1+x, y1+y))

    return points

def random_rooms (min_dim, max_dim, min_rooms, max_rooms, initial_point):
    rooms = []
    room_positions = []
    rooms_count = randint(min_rooms, max_rooms)
    for i in range(rooms_count):
        width = randint(min_dim, max_dim)
        height = randint(min_dim, max_dim)

        if (len(room_positions) != 0):
            i = randint(0, len(room_positions)-1)
            pos = room_positions[i]
        else:
            pos = initial_point

        room_bounds = bounds_from_anchor(width, height, pos[0], pos[1], AnchorPoint.centre_centre)
        room = rectangle(room_bounds, True)
        rooms.append(room)
        room_positions.extend(room)
    return rooms

def outline(points, include_diagonals=True):
    outline_points = []
    expanded_points = []
    for point in points:
        direction_list = DIRECTIONS + DIAGONALS if include_diagonals else DIRECTIONS
        for direction in direction_list:
            new_point = (direction[0] + point[0], direction[1] + point[1]) + point[2:]
            if new_point not in expanded_points:
                expanded_points.append(new_point)
    for point in expanded_points:
        if point not in points:
            outline_points.append(point)

    return outline_points

def inline(points, include_diagonals=True):
    outline_points = []
    for point in points:
        direction_list = DIRECTIONS + DIAGONALS if include_diagonals else DIRECTIONS
        for direction in direction_list:
            new_point = (direction[0] + point[0], direction[1] + point[1]) + point[2:]
            if new_point not in points and point not in outline_points:
                outline_points.append(point)

    return outline_points

def generate_house (room_max_score = 1.5, print_house = False):
    grid = Grid(30,30)
    grid.random_rooms(4, 11, 2, 3, (15, 15))
    grid.outline()
    grid.walls_from_corners()
    grid.split_rooms(10, room_max_score)
    grid.hallway()
    grid.doors()
    grid.apply_pending()

    room_types = get_room_types(len(grid._rooms) - len(grid._hallways))
    if print_house: print_grid(grid, True, room_types)

    room_types_with_hallways = []
    room_scores = []
    rooms = grid._rooms

    for room in rooms:
        if room not in grid._hallways:
            room_scores.append((room, test_rooms([room], False)))
    room_scores.sort(key = lambda room_score: room_score[1])

    for i in range(len(rooms)):
        if rooms[i] not in grid._hallways:
            index_in_score_list = 0
            for j in range(len(room_scores)):
                if room_scores[j][0] == rooms[i]:
                    index_in_score_list = j
            room_type = room_types[index_in_score_list]
        else:
            room_type = ['hallway']

        room_types_with_hallways.append(room_type)

    return grid.get_cells(), grid._rooms, room_types_with_hallways

# grid = Grid(30,30)
# grid.random_rooms(Bounds(15,15,0,0), 4, 11, 2, 3)
# grid.outline()
# grid.apply_pending()
# print_grid(grid)

# testing seeds:
# hallway: 1555906125, 1555906128
random_seed = int(time())
# random_seed = 1556631288
# random_seed = 1556705041
random_seed = 1559125456
seed(random_seed)
print ('random seed is:',random_seed)

# grid = Grid(30,30)
# grid.random_rooms(Bounds(15,15,0,0), 4, 11, 2, 3)
# grid.outline()
# grid.walls_from_corners()
# grid.apply_pending()
# print_grid(grid)

# print ('grid 2')

# grid2 = Grid(30,30)
# grid2.random_rooms(Bounds(15,15,0,0), 4, 11, 2, 3, (15, 15))
# grid2.outline()
# grid2.walls_from_corners()
# grid2.split_rooms(10, 2)
# grid2.hallway()
# grid2.apply_pending()

# room_max_score = 1.5

# grid = Grid(30,30)
# grid.random_rooms(4, 11, 2, 3, (15, 15))
# grid.outline()
# grid.walls_from_corners()
# grid.split_rooms(10, room_max_score)
# grid.hallway()
# grid.apply_pending()

# room_types = get_room_types(len(grid._rooms) - len(grid._hallways))
# print_grid(grid, True, room_types)
