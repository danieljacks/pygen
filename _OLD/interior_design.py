# TODO: implement interior design cost function described in https://graphics.stanford.edu/~pmerrell/furnitureLayout2.pdf
# then modify each piece of furniture's cost function by its importance factor,
# add the furniture piece with the highest score to the point resulting in the highest score
# then nudge each piece of furniture in each direction to get optimal results (or just use back propagation? I mean this is basically just a nueral network where the neurons are furniture locations and the error function is the score function...)
# then add the next best piece of furniture to its best spot, and repeat from previous step until all furniture placed, or there is no room for the next piece of furniture, or the cost function would be degraded by placing another piece of furniture

# implement a greedy algorithm using a scoring function with these factors:
# importance, clone importance, volume : surfacearea of free space, constraints, (assymmetry? i.e. center of mass not the same as center of mass of the room)
# like the room_names.py algorithm

from greedy import optimal_solution
from copy import deepcopy
from house_generator import DIRECTIONS, Bounds, bounds_from_points, rectangle, bounds_from_point, get_wall_symbol, COLORS, COLORS_FG
from math import tanh, sqrt
from colorama import init, Back, Fore
from colour import Color
from sty import bg, fg
# init()

# furniture categorized by function (quality of furniture is determined later)
FURNITURES = {
    # name: name, initial_importance, clone_importance, combine_tolerance
    'table': {
        'importance': 5,
        'clone_importance': 0.05,
        'margin_importance': 1,
        'min_size': {
            'length': 1,
            'width': 1
        },
        'max_size': {
            'length': 3,
            'width': 3,
        },
        'length_to_width_ratio': 2
    },
    'chair': {
        'importance': 3,
        'clone_importance': 0.8,
        'margin_importance': 0.01,
        'min_size': {
            'length': 1,
            'width': 1
        },
        'max_size': {
            'length': 1,
            'width': 2,
        },
        'length_to_width_ratio': 2,
        'constraints': {
            'table': 1 # TODO: figure out what goes here - is it a percentage of the chair's importance that is lost if it is not next to a table? or a flat amount that is added to the chair's value if it is next to a table? (im thinking the former, since it seems the value given to the chair in the table is assumed to be if all the constraints ARE actually fulfilled)
        }
    },
    'stove': {
        'importance': 4,
        'clone_importance': 0.2,
        'margin_importance': 0.8,
        'min_size': {
            'length': 1,
            'width': 1
        },
        'max_size': {
            'length': 1,
            'width': 2,
        },
        'length_to_width_ratio': 2 # TODO: implement this
    }
}

class Furniture (object):
    function = ''
    bounds = Bounds(0, 0, 0, 0)

    def __init__ (self, function, bounds):
        self.function = function
        self.bounds = bounds
    
class Orientation (object):
    bounds = Bounds (0, 0, 0, 0)
    direction = (0, 0)

    def __init__ (self, bounds, direction):
        self.bounds = bounds
        self.direction = direction

class Layout (object):
    room = []
    furnitures = [],

    def __init__ (self, room, furnitures):
        self.room = room
        self.furnitures = furnitures

def generate_layouts (current_layout):
    possible_layouts = []
    for furniture_type in FURNITURES:
        min_size, max_size = FURNITURES[furniture_type]['min_size'], FURNITURES[furniture_type]['max_size']
        orientations = generate_orientations (min_size['width'], min_size['length'], max_size['width'], max_size['length'])
        for orientation in orientations:
            for point in current_layout.room:
                check_bounds = move_bounds (orientation.bounds, point)
                if check_empty(check_bounds, current_layout.room, current_layout.furnitures):
                    if (len(possible_layouts) == 90):
                        print ('hi') # TEMP TESTING DELTE THIS IF STATEMENT
                    new_layout = deepcopy(current_layout)
                    furniture_bounds = move_bounds(orientation.bounds, point)
                    furniture = Furniture(furniture_type, furniture_bounds)
                    new_layout.furnitures.append(furniture)
                    possible_layouts.append(new_layout)
    return possible_layouts

def check_empty (check_bounds, room, furnitures):
    furnitures_bounds = [furniture.bounds for furniture in furnitures]
    room_bounds = bounds_from_points(room)
    
    for check_point in check_bounds.get_points():
        if not room_bounds.contains(check_point):
            return False

    for furniture_bounds in furnitures_bounds:
        if check_bounds.intersect(furniture_bounds):
            return False
    
    return True

def move_bounds (bounds, move_vector):
    return Bounds(  move_vector[0] + bounds.x1, 
                    move_vector[1] + bounds.y1, 
                    move_vector[0] + bounds.x2, 
                    move_vector[1] + bounds.y2 )

def generate_orientations (min_width, min_length, max_width, max_length):
    orientations = []
    for w in range(min_width, max_width + 1):
        for l in range(min_length, max_length + 1):
            #for d in DIRECTIONS:
            d = (0,1)
            orientation_bounds = width_length_to_bounds (w, l, d)
            orientation = Orientation (orientation_bounds, d)
            orientations.append (orientation)
    return orientations

def width_length_to_bounds (width, length, direction):
    if direction == (1,0) or direction == (-1, 0): # left or right
        x2, y2 = length, width
    else: # up or down
        x2, y2 = width, length
    return Bounds(0, 0, x2, y2)

def score_furniture (furniture, clones_count):
    importance = FURNITURES[furniture.function]['importance']
    clone_importance = FURNITURES[furniture.function]['clone_importance']
    return importance * clone_importance**(clones_count-1)

# returns a multiplier for the score of an object
def score_margin (score_furniture, room, furnitures):
    margin_importance = FURNITURES[score_furniture.function]['margin_importance']
    score_bounds = score_furniture.bounds
    check_radius = 5
    theoretical_score = 0
    actual_score = 0
    for r in range(check_radius):
        ring_bounds = Bounds(score_bounds.x1 - r*1, score_bounds.y1 - r*1, score_bounds.x2 + r*1, score_bounds.y2 + r*2)
        ring_points = rectangle(ring_bounds, filled=False)
        for ring_point in ring_points:
            point_score = margin_dropoff (score_bounds, ring_point, margin_importance)
            theoretical_score += point_score
            ring_point_bounds = bounds_from_point(ring_point)
            if check_empty(ring_point_bounds, room, furnitures):
                actual_score += point_score
    
    if theoretical_score == 0: return 0
    ratio = actual_score / theoretical_score
    return ratio

# raw_factor is roughly equivalent to radius of the margin that we care about (with values lower than one mostly affecting how much we care about adjacent cells)
def margin_dropoff (bounds, point, raw_factor):
    center = (bounds.x1 + 0.5 * bounds.width, bounds.y1 + 0.5 * bounds.height)
    dist = sqrt( (center[0] - point[0])**2 + (center[1] - point[1])**2 )
    sqrt_factor = sqrt(raw_factor)
    return sqrt_factor * (1 - tanh( (dist - 0.5) / sqrt_factor )) # softmax (tanh) weighted to get more intuitive values, so that the margin importance roughly corresponds to both the dropoff distance and the size of the max margin value at 0

def score_layout (layout):
    furnitures = layout.furnitures
    furniture_count = {Furniture.function: 0 for Furniture in furnitures}
    score = 0
    for furniture in furnitures:
        furniture_count[furniture.function] += 1
        importance = score_furniture (furniture, furniture_count[furniture.function])
        margin_multiplier = score_margin (furniture, layout.room, furnitures)
        score += importance * margin_multiplier
    return score

def generate_furniture_colors (furnitures, hue):
    count = len(furnitures)
    color_vals = [Color(hue=hue, luminance=0.5, saturation=i/count) for i in range(count)]
    color_codes = [bg(round(color_vals[i].red * 255), 0, round(color_vals[i].blue * 255)) for i in range(count)]
    return color_vals

def draw_layout (layout):
    room_bounds = bounds_from_points(layout.room)
    furnitures = layout.furnitures
    colors = generate_furniture_colors(layout.furnitures, 0)
    furniture_map = {}
    for i in range(len(furnitures)):
        for point in furnitures[i].bounds.get_points():
            furniture_map[point[0], point[1]] = i

    for y in range(room_bounds.y1, room_bounds.y2 + 1):
        row = ''
        for x in range(room_bounds.x1, room_bounds.x2 + 1):
            point = (x, y)
            if point in furniture_map:
                item_color = COLORS[furniture_map[point] + 3]
                row += item_color + ' ' + Back.RESET
            else:
                row += COLORS[-1] + ' ' + Back.RESET
        print (row)

    print ('score: ' + str(score_layout(layout)))
    draw_legend(layout)
            
def draw_legend (layout):
    for i in range(len(layout.furnitures)):
        furniture = layout.furnitures[i]
        color = COLORS_FG[i + 3]
        string = str(i) + ': ' + furniture.function
        print (color + string + Fore.RESET)

# function which takes bounds, and a list of furnitures, where each furniture is given a min and max size, and a score, then output the room layout with the best score

initial_room = Bounds (0, 0, 8, 8).get_points()
initial_furniture = Furniture ('table', Bounds(12, 12, 14, 15))
initial_layout = Layout (initial_room, [])
draw_layout(initial_layout)
new_furniture = Furniture('chair', Bounds(11,11,11,11))
# initial_layout.furnitures.append(new_furniture)
print ('--')
draw_layout(initial_layout)

# possible_layouts = generate_layouts(initial_layout)
# for i in range(90, 100):
#     draw_layout (possible_layouts[i])
#     print ('------------------' + str(i))
print ('optimal: ')
solution_layout = optimal_solution(initial_layout, generate_layouts, score_layout)
draw_layout(solution_layout)

# first choose the best spot, then look at each furniture, and test if it is good or not