from classes.rect import Rect
from classes.entity import Entity
from classes.position import Position

# a body is an entity that can be drawn
class Body(Entity):
    position = Position(0, 0) # top left corner
    width = 0
    height = 0
    materials = []
    direction = 0

    @property
    def bounds(self):
        return Rect(self.position.x, self.position.y, self.position.x + self.width, self.position.y + self.height)

    def __init__(self, position, rotation, width, height, materials):
        self.position = position
        self.rotation = rotation
        self.width = width
        self.height = height
        self.materials = materials